﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


using System;

using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;

namespace Project1
{
    // Use this namespace here in case we need to use Direct3D11 namespace as well, as this
    // namespace will override the Direct3D11.
    using SharpDX.Toolkit.Graphics;

    public class Project1Game : Game
    {
        private GraphicsDeviceManager graphicsDeviceManager;
        private GameObject landscape;
        private GameObject sun;
        public Camera camera;
        public KeyboardManager keyboardManager;
        public KeyboardState keyboardState;
        private MouseManager mouseManager;
        private MouseState mouseState;
        private Vector2 screenCentre;


        /// <summary>
        /// Initializes a new instance of the <see cref="Project1Game" /> class.
        /// </summary>
        public Project1Game()
        {
            // Creates a graphics manager. This is mandatory.
            graphicsDeviceManager = new GraphicsDeviceManager(this);

            keyboardManager = new KeyboardManager(this);
            mouseManager = new MouseManager(this);
            screenCentre = new Vector2(0.5f, 0.5f);
            

            // Setup the relative directory to the executable directory
            // for loading contents with the ContentManager
            Content.RootDirectory = "Content";
        }

        protected override void LoadContent()
        {

            camera = new Camera(this);
            landscape = new Landscape(this);
            float landscapeWidth = ((Landscape)landscape).landscapeWidth;

            // put sun at an unreasonably lower height so that it can be seen when zoomed out
            float sunHeight = (float)(Math.Abs(((Landscape)landscape).minHeight) + Math.Abs(((Landscape)landscape).maxHeight));

            sun = new Sun(this, landscapeWidth, sunHeight); 

            // Create an input layout from the vertices

            base.LoadContent();
        }

        protected override void Initialize()
        {
            Window.Title = "Project 1";
            
            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {

            keyboardState = keyboardManager.GetState();
            mouseState = mouseManager.GetState();
            mouseManager.SetPosition(screenCentre);

            float timeDelta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float speed = 50f; // speed of rolling and moving
            float mouseSpeed = 250f; // speed of mouse controls

            
            // change in positions of mouse
            float dX = mouseState.X - screenCentre.X;
            float dY = mouseState.Y - screenCentre.Y; 

            // rotate around the up vector of camera by angle dependent on mouse movement for yaw
            Quaternion rotateYaw = Quaternion.RotationAxis(camera.Up, MathUtil.DegreesToRadians(mouseSpeed * timeDelta * dX));
            if (dX > 0) {
                camera.cameraTarget = Vector3.Transform(camera.cameraTarget, rotateYaw);
                camera.Update();
            }
            else if (dX < 0) {
                //camera.Up = Vector3.Transform(camera.Up, rotateYawCW);
                camera.cameraTarget = Vector3.Transform(camera.cameraTarget, rotateYaw);
                camera.Update();
            }

            Vector3 left = Vector3.Normalize(Vector3.Cross(camera.viewVector, camera.Up)); // left of the camera
            Vector3 right = -1 * left; // left of the camera

            // rotate around the right/left vector of the camera to change pitch
            Quaternion rotatePitch = Quaternion.RotationAxis(right, MathUtil.DegreesToRadians(mouseSpeed * timeDelta * dY));
            if (dY > 0) {
                camera.cameraTarget = Vector3.Transform(camera.cameraTarget, rotatePitch);
                camera.Up = Vector3.Transform(camera.Up, rotatePitch);
                camera.Update();
            }
            else if (dY < 0) {
                //camera.Up = Vector3.Transform(camera.Up, rotateYawCW);
                camera.cameraTarget = Vector3.Transform(camera.cameraTarget, rotatePitch);
                camera.Up = Vector3.Transform(camera.Up, rotatePitch);
                camera.Update();
            }


            // move forwards or backwards
            if (keyboardState.IsKeyDown(Keys.W)) {
                camera.cameraPos += speed * timeDelta* camera.viewVector;
                camera.AdjustCamera();
                camera.Update();
            }
            if (keyboardState.IsKeyDown(Keys.S)) {
                camera.cameraPos -= speed * timeDelta * camera.viewVector;
                camera.AdjustCamera();
                camera.Update();
            }


            // calculate right/left vectors again (mouse movement may have changed camera properties)
            left = Vector3.Normalize(Vector3.Cross(camera.viewVector, camera.Up));
            right = -1 * left;

            // move camera left or right
            if (keyboardState.IsKeyDown(Keys.A)) {
                camera.cameraPos += speed * timeDelta * left;
                camera.cameraTarget += speed * timeDelta * left;
                camera.AdjustCamera();
                camera.Update();
            }
            if (keyboardState.IsKeyDown(Keys.D)) {
                camera.cameraPos += speed * timeDelta * right;
                camera.cameraTarget += speed * timeDelta * right;
                camera.AdjustCamera();

                camera.Update();
            }

            // rotate clockwise or counter clockwise around the direction the camera is facing
            Quaternion rotateCW = Quaternion.RotationAxis(camera.viewVector, MathUtil.DegreesToRadians(-1 * speed * timeDelta));
            if (keyboardState.IsKeyDown(Keys.Q)) {
                camera.Up = Vector3.Transform(camera.Up, rotateCW);
                camera.Update();
            }

            Quaternion rotateCCW = Quaternion.RotationAxis(camera.viewVector, MathUtil.DegreesToRadians(1 * speed * timeDelta));
            if (keyboardState.IsKeyDown(Keys.E)) {
                camera.Up = Vector3.Transform(camera.Up, rotateCCW);
                camera.Update();
            }

            // exit game
            if (keyboardState.IsKeyDown(Keys.Escape)) {
                this.Exit();
                this.Dispose();
            }
            landscape.Update(gameTime);
            sun.Update(gameTime);

            // Handle base.Update
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            // Clears the screen with the Color.CornflowerBlue
            GraphicsDevice.Clear(Color.CornflowerBlue);

            landscape.Draw(gameTime);
            sun.Draw(gameTime);

            // Handle base.Draw
            base.Draw(gameTime);
        }
    }
}
