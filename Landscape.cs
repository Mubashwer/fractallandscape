﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace Project1
{
    using SharpDX.Toolkit.Graphics;
    class Landscape : ColoredGameObject
    {
        private float[,] heightMap; //heightMap for the landscape
        public int landscapeWidth; // width of the landscape (2^detail +1)
        private int detail = 8; // DECREASE IT IF FPS IS TOO LOW (number of iterations of diamond square algorithm)
        public float maxHeight; 
        public float minHeight;
        private const int verticesPerPolygon = 3;
        private const int verticesPerWorldUnit = 2 * verticesPerPolygon;
        Camera camera; // camera reference

        public Landscape(Project1Game game)
        {

            // width of landscape for diamond square algorithm to work
            landscapeWidth = (int)Math.Pow(2, detail) + 1;
            int totalVertices = ((int)Math.Pow((landscapeWidth - 1), 2) * verticesPerWorldUnit);
            
            // add water vertices
            totalVertices = totalVertices * 6;

            VertexPositionNormalColor[] vertexList = new VertexPositionNormalColor[totalVertices];

            // fractal generation algorithm
            DiamondSquare ds = new DiamondSquare(landscapeWidth);
            heightMap = ds.giveHeightMap();

            // heightMap statistics
            IEnumerable<float> v = heightMap.Cast<float>();
            float min = v.Min();
            float max = v.Max();
            float avg = v.Average();
            float mid = (min + max) / 2.0f;
            maxHeight = min;
            minHeight = max;
            Console.WriteLine("min = " + min + " max = " + max + " avg = " + avg);

            // water vertice properties
            float waterHeight = avg;
            Color waterColour = new Color(0, 64, 128, 200);


            // build land square by square where each square has two triangles
            // n.b. map generated is reverse (i.e. the mininum heightMap is the maximum and vice versa)
            int i = 0;
            for (int x = 0; x < landscapeWidth - 1; x++)
                for (int y = 0; y < landscapeWidth - 1; y++)
                {

                    // Vectors for 4 vertices of a square
                    Vector3 v1 = new Vector3(x, y, heightMap[x, y]);
                    Vector3 v2 = new Vector3(x, y + 1, heightMap[x, y + 1]);
                    Vector3 v3 = new Vector3(x + 1, y, heightMap[x + 1, y]);
                    Vector3 v4 = new Vector3(x + 1, y + 1, heightMap[x + 1, y + 1]);

                    // Divide square into left and right triangles and add the vertices
                    Vector3 leftTriangleNormal = (Vector3.Cross(v1 - v2, v3 - v1));

                    vertexList[i++] = new VertexPositionNormalColor(v1, leftTriangleNormal, HeightToColor(min, max, avg, heightMap[x, y]));
                    vertexList[i++] = new VertexPositionNormalColor(v2, leftTriangleNormal, HeightToColor(min, max, avg, heightMap[x, y + 1]));
                    vertexList[i++] = new VertexPositionNormalColor(v3, leftTriangleNormal, HeightToColor(min, max, avg, heightMap[x + 1, y]));

                    Vector3 rightTriangleNormal = (Vector3.Cross(v3 - v2, v4 - v3));
                    vertexList[i++] = new VertexPositionNormalColor(v3, rightTriangleNormal, HeightToColor(min, max, avg, heightMap[x + 1, y]));
                    vertexList[i++] = new VertexPositionNormalColor(v2, rightTriangleNormal, HeightToColor(min, max, avg, heightMap[x, y + 1]));
                    vertexList[i++] = new VertexPositionNormalColor(v4, rightTriangleNormal, HeightToColor(min, max, avg, heightMap[x + 1, y + 1]));

                }


            // Flat surface of water within the landscape
            Vector3 waterNormal = new Vector3(0, 0, 1);
            vertexList[i++] = new VertexPositionNormalColor(new Vector3(0, 0, waterHeight + 1), waterNormal, waterColour);
            vertexList[i++] = new VertexPositionNormalColor(new Vector3(0, landscapeWidth, waterHeight), waterNormal, waterColour);
            vertexList[i++] = new VertexPositionNormalColor(new Vector3(landscapeWidth, 0, waterHeight), waterNormal, waterColour);
            vertexList[i++] = new VertexPositionNormalColor(new Vector3(0, landscapeWidth, waterHeight), waterNormal, waterColour);
            vertexList[i++] = new VertexPositionNormalColor(new Vector3(landscapeWidth, landscapeWidth, waterHeight), waterNormal, waterColour);
            vertexList[i++] = new VertexPositionNormalColor(new Vector3(landscapeWidth, 0, waterHeight), waterNormal, waterColour);

            vertices = Buffer.Vertex.New(
                            game.GraphicsDevice,
                            vertexList
                   );
            this.game = game;

            SetupCamera(); // adjust camera to start properly

            basicEffect = new BasicEffect(game.GraphicsDevice)
            {
                VertexColorEnabled = true,
                View = camera.View,
                Projection = camera.Projection,
                World = Matrix.Identity
            };

            // lighting effects
            basicEffect.LightingEnabled = true;
            basicEffect.AmbientLightColor = new Vector3(0.1f, 0.1f, 0.1f);
            basicEffect.DirectionalLight0.SpecularColor = new Vector3(0.75f, 0.75f, 0.75f);
            basicEffect.DirectionalLight0.DiffuseColor = new Vector3(0.75f, 0.75f, 0.75f);

            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            
        }

        public override void Update(GameTime gameTime)
        {

            var time = (float)gameTime.TotalGameTime.TotalSeconds;
            basicEffect.View = camera.View;
            basicEffect.Projection = camera.Projection;

            float movingFactor = 0.25f * time; // determines speed of light
            
            // Move light diagonally around the landscape just like the sun
            basicEffect.DirectionalLight0.Direction = new Vector3((float)Math.Sin(movingFactor), (float)Math.Sin(movingFactor), (float)Math.Cos(movingFactor));

        }

        public override void Draw(GameTime gameTime)
        {
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);
            game.GraphicsDevice.SetBlendState(game.GraphicsDevice.BlendStates.AlphaBlend); // for transparency
            basicEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        // adjusts the camera to that it is properly initialized
        void SetupCamera()
        {
            camera = game.camera;
            // camera faces up but it is rotated immediately towards landscape
            camera.cameraPos = new Vector3(landscapeWidth / 2.0f, landscapeWidth / 2.0f, maxHeight - (landscapeWidth*2));
            camera.cameraTarget = new Vector3(landscapeWidth / 2.0f, landscapeWidth / 2.0f, maxHeight - (landscapeWidth*3));
            // set camera boundaries 
            camera.SetBoundary(new Vector3(0, 0, -800f), new Vector3(landscapeWidth - 1, landscapeWidth - 1, maxHeight));
            camera.Update();

            // initially camera faces to the opposite direction, so rotate it towards the land and change its orientation
            // and target appropriately
            Vector3 right = Vector3.Normalize(Vector3.Cross(camera.viewVector, camera.Up)); // right of up
            Quaternion rotatePitchUp = Quaternion.RotationAxis(right, MathUtil.DegreesToRadians(180));
            camera.cameraTarget = Vector3.Transform(camera.cameraTarget, rotatePitchUp);
            camera.Up = Vector3.Transform(camera.Up, rotatePitchUp);
            camera.Update();

        }

        // Calculates the colour terrain  given height (n.b. negative height values are peaks)
        private Color HeightToColor(float min, float max, float avg, float height)
        {

            float totalHeight = Math.Abs(min) + Math.Abs(max);
            float heightAbs = Math.Abs(height - min);
            int fractions = 6;

            // divides total height into fractions and then performs smooth transition of
            // of color from dark green to white
            float heightFraction = totalHeight * (1.0f / fractions);

            if (heightAbs < heightFraction)
                return Color.SmoothStep(Color.White, Color.Brown, heightAbs / heightFraction);

            if (heightAbs < heightFraction * 2)
                return Color.SmoothStep(Color.Brown, Color.SandyBrown, (heightAbs - heightFraction) / heightFraction);

            if (heightAbs < heightFraction * 3)
                return Color.SmoothStep(Color.SandyBrown, Color.LightGreen, (heightAbs - heightFraction * 2) / (heightFraction));

            if (heightAbs < heightFraction * 4)
                return Color.SmoothStep(Color.LightGreen, Color.SeaGreen, (heightAbs - (heightFraction * 3)) / heightFraction);

            if (heightAbs < heightFraction * 5)
                return Color.SmoothStep(Color.SeaGreen, Color.DarkSeaGreen, (heightAbs - (heightFraction * 4)) / heightFraction);

            return Color.SmoothStep(Color.DarkSeaGreen, Color.DarkGreen, (heightAbs - (heightFraction * 5)) / heightFraction);

        }

    }
}
