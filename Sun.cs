﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

/*

    With thanks to Matt Blair for the skelton code taken from the cube.cs class in Lab5.
    Taken and modified for our purposes as nice square sun!

*/

namespace Project1
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    class Sun : ColoredGameObject // initial template was from a lab
    {
        private float widthHalf; // half of the width of the cube
        private float radius; // radius of revolution 
        private float sunHeight; // maximum height of sun
        private Vector3 position; // position of the sun
       
        public Sun(Project1Game game, float landWidth, float sunHeight)
        {
            position = Vector3.Zero;

            // normals
            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -widthHalf);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, widthHalf);
            Vector3 topNormal = new Vector3(0.0f, widthHalf, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -widthHalf, 0.0f);
            Vector3 leftNormal = new Vector3(-widthHalf, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(widthHalf, 0.0f, 0.0f);

            
            widthHalf = landWidth / 6.0f;
            radius = (float)Math.Sqrt(Math.Pow(landWidth, 2) + Math.Pow(landWidth, 2));
            this.sunHeight = sunHeight;

            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, -widthHalf), frontNormal, Color.Orange), // Front
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, -widthHalf), frontNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, -widthHalf), frontNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, -widthHalf), frontNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, -widthHalf), frontNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, -widthHalf), frontNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, widthHalf), backNormal, Color.Orange), // BACK
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, widthHalf), backNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, widthHalf), backNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, widthHalf), backNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, widthHalf), backNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, widthHalf), backNormal, Color.Orange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, -widthHalf), topNormal, Color.OrangeRed), // Top
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, widthHalf), topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, widthHalf), topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, -widthHalf), topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, widthHalf), topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, -widthHalf), topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, -widthHalf), bottomNormal, Color.OrangeRed), // Bottom
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, widthHalf), bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, widthHalf), bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, -widthHalf),bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, -widthHalf), bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, widthHalf), bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, -widthHalf), bottomNormal, Color.DarkOrange), // Left
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, widthHalf), leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, widthHalf), leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, -widthHalf, -widthHalf), leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, widthHalf), leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(-widthHalf, widthHalf, -widthHalf), leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, -widthHalf), rightNormal, Color.DarkOrange), // Right
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, widthHalf), rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, widthHalf), rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, -widthHalf, -widthHalf), rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, -widthHalf), rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(new Vector3(widthHalf, widthHalf, widthHalf), rightNormal, Color.DarkOrange),
                });

            basicEffect = new BasicEffect(game.GraphicsDevice)
            {
                VertexColorEnabled = true,
                View = game.camera.View,
                Projection = game.camera.Projection,
                World = Matrix.Identity
            };

            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {

            var time = (float)gameTime.TotalGameTime.TotalSeconds;
            float moveFactor = 0.25f; // determines the speed of sun revolution

            // move the sun diagonally around the land 
            this.position = new Vector3(radius* -(float)Math.Sin(moveFactor * time), radius * -(float)Math.Sin(moveFactor * time), sunHeight* (float)Math.Cos(moveFactor * time));
            basicEffect.World = Matrix.Translation(this.position);
            basicEffect.View = game.camera.View;
            basicEffect.Projection = game.camera.Projection;

        }

        public override void Draw(GameTime gameTime)
        {
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            // Apply the basic effect technique and draw the rotating cube
            basicEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }
    }
}
