﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
   
    Class to generate the landscape. Uses the Diamond Square pattern
    to generate an adjustable fractal landscape

*/
namespace Project1
{
    
    class DiamondSquare
    {
        // height map of landscape
        float[,] heightMap;
        // size of the landscape (must be of form 2^n + 1)
        int width;
        // maximum number of squares in height map
        int maxSize;
        // determines whether terrain is smooth (close to 0) or mountainous (close to 1) 
        float roughnessConstant;
        // seed value for corner heights
        float cornerSeed;
        // random number generator
        Random rand;

        public DiamondSquare(int width)
            : this(width, 0.3f, 0.3f)
        {
        }


        public DiamondSquare(int width, float roughnessConstant, float cornerSeed)
        {
            this.heightMap = new float[width, width]; 
            this.rand = new Random();
            this.roughnessConstant = roughnessConstant;
            this.width = width;
            this.cornerSeed = cornerSeed;
            this.maxSize = width - 1;
            
        }

        public float[,] giveHeightMap()
        {

            // seed corners
            heightMap[0, 0] = cornerSeed;
            heightMap[0, maxSize] = cornerSeed;
            heightMap[maxSize, 0] = cornerSeed;
            heightMap[maxSize, maxSize] = cornerSeed;
            
            applyDiamondSquareAlgorithm(maxSize);
            
            return heightMap;
        }
        
        /*

            Apply Diamond Square Algorithm
            Performs Diamond steps and Square steps recursively

        */

        private void applyDiamondSquareAlgorithm(int size)
        {
            int half = size/2;
            if (half < 1) return;
            float roughness = roughnessConstant * size; // decrease it with each iteration

            // go through the centre of all the squares of given
            // size and perform diamond and 4 square steps for each
            for (int x = half; x < maxSize; x += size)
                for (int y = half; y < maxSize; y += size) 
                {
                    performDiamondStep(x, y, randomFloat(roughness), half);
                    performSquareStep(x - half, y, randomFloat(roughness), half);
                    performSquareStep(x + half, y, randomFloat(roughness), half);
                    performSquareStep(x, y - half, randomFloat(roughness), half);
                    performSquareStep(x, y + half, randomFloat(roughness), half);
                }

            applyDiamondSquareAlgorithm(half);
        }

        // Square step Algorithm

        private void performSquareStep(int x, int y, float offset, int size)
        {
            float totalHeight = 0;
            int count = 0;
            if(!OutOfBound(heightMap, x-size, y, width))
            {   
                totalHeight += heightMap[x-size,y];
                count++;
            }
            if(!OutOfBound(heightMap, x+size, y, width))
            {
                totalHeight += heightMap[x+size,y];
                count++;
            }
            if(!OutOfBound(heightMap, x, y-size, width))
            {
                totalHeight += heightMap[x,y-size];
                count++;
            }
            if(!OutOfBound(heightMap, x, y+size, width))
            {
                totalHeight += heightMap[x,y+size];
                count++;
            }

            heightMap[x,y] = (totalHeight/count) + offset;

        }

        // Diamond Step Algorithm

        private void performDiamondStep(int x, int y, float offset, int size)
        {
            float totalHeight = 0;
            int count = 0;
            if(!OutOfBound(heightMap, x-size, y-size, width))
            {   
                totalHeight += heightMap[x-size,y-size];
                count++;
            }
            if(!OutOfBound(heightMap, x-size, y+size, width))
            {
                totalHeight += heightMap[x-size,y+size];
                count++;
            }
            if(!OutOfBound(heightMap, x+size, y-size, width))
            {
                totalHeight += heightMap[x+size,y-size];
                count++;
            }
            if(!OutOfBound(heightMap, x+size, y+size, width))
            {
                totalHeight += heightMap[x+size,y+size];
                count++;
            }

            heightMap[x,y] = (totalHeight/count) + offset;

        }

        // Function to return whether or not out of Bounds

        private bool OutOfBound(float[,] heightMap, float x, float y, int width)
        {
            return (x >= width || x < 0 || y >= width || y < 0);
        }

        // returns a random float within the range [-num,num)
        float randomFloat(float num)
        {
            return (float)((rand.NextDouble() * 2 * num) - num);
        }

    }
}
