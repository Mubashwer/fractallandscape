﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;


/*
    With Thanks to Matt Blair for the original Camera class from Lab 5, taken and modified for our purposes
*/

namespace Project1
{
    public class Camera // initial template was from a lab
    {
        public Matrix View;
        public Matrix Projection;
        public Game game;
        public Vector3 cameraPos; // position of camera
        public Vector3 cameraTarget; // where the camera is facing
        public Vector3 viewVector; //direction of camera
        public Vector3 Up; // orientation of camera

        // minimum and maximum boundaries for camera position
        private Vector3 minCamPos; 
        private Vector3 maxCamPos;
        private bool boundSet;

        // Ensures that all objects are being rendered from a consistent viewpoint
        public Camera(Game game)
        {
            cameraPos = new Vector3(0, 0, -10);
            cameraTarget = new Vector3(0, 0, 0);
            viewVector = Vector3.Normalize(cameraTarget - cameraPos);
            Up = Vector3.UnitY;
            View = Matrix.LookAtLH(cameraPos, cameraTarget, Up);
            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 1000.0f);
            this.game = game;
            boundSet = false;
        }

        // If the screen is resized, the projection matrix will change
        public void Update()
        {
            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 1000.0f);
            View = Matrix.LookAtLH(cameraPos, cameraTarget, Up);
            viewVector = Vector3.Normalize(cameraTarget - cameraPos);
        }

        // Sets optional boundaries for camera position
        public void SetBoundary(Vector3 min, Vector3 max) {
            minCamPos = min;
            maxCamPos = max;
            boundSet = true;
        }

        // If the camera crosses the given boundaries then
        // it is not allowed (by moving it back to the boundary)
        public void AdjustCamera()
        {

            if (!boundSet) return;
            if (cameraPos.X <= minCamPos.X)
            {
                cameraPos.X = minCamPos.X;
            }
            else if (cameraPos.X >= maxCamPos.X)
            {
                cameraPos.X = maxCamPos.X;
            }

            if (cameraPos.Y < minCamPos.Y)
            {
                cameraPos.Y = minCamPos.Y;
            }
            else if (cameraPos.Y >= maxCamPos.X)
            {
                cameraPos.Y = maxCamPos.X;
            }

            if (cameraPos.Z >= maxCamPos.Z)
            {
                cameraPos.Z = maxCamPos.Z;
            }
            else if (cameraPos.Z <= minCamPos.Z)
            {
                cameraPos.Z = minCamPos.Z;
            }
        }
    }
}
