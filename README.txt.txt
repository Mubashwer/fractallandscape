Mubashwer Salman Khurshid (mskh, 601738)
Geordie Wicks (gwicks, 185828)

Program to create a fractual landscape using the diamond square algorithm. Uses ambient, diffuse and specular lighting effects

Use W, S, A and K keys for movement. Q and E keys perform rotation.
The mouse is used for pitch and yaw of users perspective, Keys movement based around user perspective

Some Notes:

The Camera.cs and Sun.cs were taken from our Lab classes and modified to suit this application. The original skeleton was written for us by Matt Blair (I think!)

The heightMap is reversed i.e. high negative values are peaks of mountains...

Change the detail in the Landscape to increase number of iterations for the diamond square algorithm and width. It slows down if it is increased more than 8. If it is slow, decrease it to 7 or lower.

Sun
The sun revolves around the landscape diagonally mimicking the light direction movement of the landscape. We have deliberate lowered its height so that it can be easily seen.
The initial camera position is placed very high so that sun can be seen. Move down to explore the landscape.

The landscape
The landscape is reversed i.e. high negative values are peaks of mountains... It starts from (0,0,z) to (landscapeWith-1, landscapeWidth-1, z'). The unit of landscape
is a square and each square is made of two triangles. Water surface of the same size is inserted inside the landscape, so some parts are submerged and some are not.

Diamond Square Algorithm
A level of detail is given, say 16. It is recursively divided by 2 until it less than 1, and for each stage, one diamondstep and 4 square steps are performed.
The diamond steps finds centre the average of the centre of square of size (current detail / 2) within the map. For each diamond stage, you get 4 diamonds. Then
the average of the diamond is calculated. An offset is added to both the averages which is the current detail multiplied by the roughness constant.

